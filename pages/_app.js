/* eslint-disable react/jsx-props-no-spreading */
import App from 'next/app'
import React from 'react'
import { ThemeProvider } from 'styled-components'
import { ApolloProvider } from '@apollo/react-hooks'
import withData from '../util/apollo-client'

const theme = {
  primary: 'green'
}
class MyApp extends App {
  // remove it here
  componentDidMount() {
    const jssStyles = document.querySelector('#jss-server-side')
    if (jssStyles && jssStyles.parentNode) jssStyles.parentNode.removeChild(jssStyles)
  }

  render() {
    const { Component, pageProps, apollo } = this.props
    return (
      <ThemeProvider theme={theme}>
        <ApolloProvider client={apollo}>
          <Component {...pageProps} />
        </ApolloProvider>
      </ThemeProvider>
    )
  }
}
// Wraps all theme, data and nextapp components in the tree with the data provider
export default withData(MyApp)
